package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func getPage() *goquery.Document {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://www.reddit.com/r/wallpapers/hot", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "Mozilla/5.0")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	document, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return document
}

func search(index int, element *goquery.Selection) {
	img, exists := element.Attr("src")
	if exists {
		filter(img)
	}
}

func filter(site string) {
	string1 := "preview"
	string2 := "award"
	parsed, _ := url.Parse(site)
	if strings.Contains(parsed.Host, string1) && !strings.Contains(parsed.Path, string2) {
		fmt.Printf(site + "\n")
	}
}

func main() {
	document := getPage()
	document.Find("img").Each(search)
}
